<?php
/*
Plugin Name: Ishtar
Description: An anime-themed front end framework.
Version: 1.0
License: GPL-2.0+
*/

use Gilgamesh\Service\RegisterExtensionService;
use Ishtar\Controller;

defined('ABSPATH') || exit;
if (! defined('USE_COMPOSER_AUTOLOADER') || ! USE_COMPOSER_AUTOLOADER) {
    require __DIR__ . '/vendor/autoload.php';
}

/**
 * Class Ishtar
 * @author  Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class Ishtar
{
    const EXTENSIONS = [
        Controller\IconController::class
    ];

    const THEME_SUPPORT = [
        'disable-custom-colors',
        'disable-custom-font-sizes',
        'disable-custom-gradients'
    ];
    public function __construct()
    {
        add_action('gilgamesh/init', function() {
            // TODO: Consider making IshtarController.php
            array_map('add_theme_support', static::THEME_SUPPORT);
            add_theme_support('editor-font-sizes', []);
            add_theme_support('editor-gradient-presets', []);
            add_theme_support('editor-color-palette', $this->getColorPalette());
            (new Ishtar\DesignCustomizer())->build();

            add_action('init', function() {
                (new RegisterExtensionService(static::EXTENSIONS))->run();
            });
        });
    }

    public function getColorPalette()
    {
        return [
            [
                'name'  => 'Primary',
                'slug'  => 'primary',
                'color' => get_theme_mod('global__colors__primary'),
            ],
            [
                'name'  => 'Primary Dark',
                'slug'  => 'primary-dark',
                'color' => get_theme_mod('global__colors__primary-dark'),
            ],
            [
                'name'  => 'Primary Light',
                'slug'  => 'primary-light',
                'color' => get_theme_mod('global__colors__primary-light'),
            ],
            [
                'name'  => 'Secondary',
                'slug'  => 'secondary',
                'color' => get_theme_mod('global__colors__secondary'),
            ],
            [
                'name'  => 'Secondary',
                'slug'  => 'secondary-dark',
                'color' => get_theme_mod('global__colors__secondary-dark'),
            ],
            [
                'name'  => 'Secondary',
                'slug'  => 'secondary-light',
                'color' => get_theme_mod('global__colors__secondary-light'),
            ],
            [
                'name'  => 'Gray Darker',
                'slug'  => 'gray-darker',
                'color' => get_theme_mod('global__colors__gray-darker'),
            ],
            [
                'name'  => 'Gray Dark',
                'slug'  => 'gray-dark',
                'color' => get_theme_mod('global__colors__gray-dark'),
            ],
            [
                'name'  => 'Gray Medium',
                'slug'  => 'gray-medium',
                'color' => get_theme_mod('global__colors__gray-medium'),
            ],
            [
                'name'  => 'Gray Light',
                'slug'  => 'gray-light',
                'color' => get_theme_mod('global__colors__gray-light'),
            ],
            [
                'name'  => 'Gray Lighter',
                'slug'  => 'gray-lighter',
                'color' => get_theme_mod('global__colors__gray-lighter'),
            ],
            [
                'name'  => 'White',
                'slug'  => 'white',
                'color' => get_theme_mod('global__colors__white'),
            ],
            [
                'name'  => 'Black',
                'slug'  => 'black',
                'color' => get_theme_mod('global__colors__black'),
            ],
        ];
    }
}

new Ishtar();