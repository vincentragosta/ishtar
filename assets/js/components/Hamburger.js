/**
 * class Hamburger
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class Hamburger {
    constructor() {
        this.activeClass = 'hamburger--active';
        this.targetActiveClass = 'primary-navigation--active';
        this.lockScroll = 'lock-scroll';

        let blockLoaded = false;
        let blockLoadedInterval = setInterval(() => {
            let hamburger = document.getElementById('hamburger');
            if (hamburger) {
                let target = document.getElementById('primary-navigation');
                let body = document.querySelector('body');
                hamburger.addEventListener('click', (event) => {
                    event.target.classList.toggle(this.activeClass, !event.target.classList.contains(this.activeClass));
                    if (target) {
                        body.classList.toggle(this.lockScroll, !body.classList.contains(this.lockScroll));
                        target.classList.toggle(this.targetActiveClass, !target.classList.contains(this.targetActiveClass));
                    }
                });
                blockLoaded = true;
            }
            if (blockLoaded) {
                clearInterval(blockLoadedInterval);
            }
        }, 500);
    }
}

/** TODO: consider making an extension service class so you can always export default here **/
new Hamburger();
