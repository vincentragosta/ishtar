/**
 * class Viewport
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class Viewport {
    static isInViewport(element) {
        const {top, bottom} = element.getBoundingClientRect();
        const vHeight = (window.innerHeight || document.documentElement.clientHeight);
        return (
            (top > 0 || bottom > 0) &&
            top < vHeight
        );
    }

    static isEntireElementInViewport(element) {
        const {top, left, bottom, right} = element.getBoundingClientRect();
        return (
            top >= 0 &&
            left >= 0 &&
            bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    };
}

export default Viewport;