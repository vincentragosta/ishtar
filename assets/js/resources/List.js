/**
 * TODO: Move to Ishtar
 *
 * class HeaderNav
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class HeaderNav {
    constructor() {
        let blockLoaded = false;
        let blockLoadedInterval = setInterval(() => {
            let hasSubMenu = document.getElementsByClassName('has-sub-menu');
            if (hasSubMenu) {
                let items = Array.prototype.slice.call(hasSubMenu);
                items.forEach((value) => {
                    value.addEventListener('click', () => {
                        value.classList.toggle('active', !value.classList.contains('active'));
                    });
                });
                blockLoaded = true;
            }
            if (blockLoaded) {
                clearInterval(blockLoadedInterval);
            }
        }, 500);
    }
}

new HeaderNav();
