/** TODO: Change to a class **/

const {registerBlockStyle} = wp.blocks;

registerBlockStyle('core/list', {
    name: 'default',
    label: 'Default',
    isDefault: true
});

registerBlockStyle('core/list', {
    name: 'unstyled',
    label: 'Unstyled',
});

registerBlockStyle('core/list', {
    name: 'inline',
    label: 'Inline'
});
