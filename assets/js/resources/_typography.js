/** TODO: Change to a class **/

const {addFilter} = wp.hooks;
const {__} = wp.i18n;
const {createHigherOrderComponent} = wp.compose;
const {Fragment} = wp.element;
const {InspectorControls} = wp.blockEditor;
const {PanelBody, SelectControl} = wp.components;

const enableSizingControls = [
    'core/heading',
    'core/paragraph',
    'core/list'
];

const sizingControlOptions = [
    {
        label: __('Select Size'),
        value: ''
    },
    {
        label: __('Small'),
        value: 'small',
    },
    {
        label: __('Regular'),
        value: 'regular',
    },
    {
        label: __('Large'),
        value: 'large',
    },
    {
        label: __('Extra Large'),
        value: 'xlarge',
    },
];

/**
 * Add sizing control attribute to block.
 *
 * @param {object} settings Current block settings.
 * @param {string} name Name of block.
 *
 * @returns {object} Modified block settings.
 */
const addSizingControlAttribute = (settings, name) => {
    if (!enableSizingControls.includes(name)) {
        return settings;
    }

    settings.attributes = {...settings.attributes, ...{
            size: {
                type: 'string',
                default: sizingControlOptions[0].value,
            }
    }};

    return settings;
};
addFilter('blocks.registerBlockType', 'ishtar/attribute/size', addSizingControlAttribute);

/**
 * Create HOC to add spacing control to inspector controls of block.
 */
const withSizingControl = createHigherOrderComponent((BlockEdit) => {
    return (props) => {
        if (!enableSizingControls.includes(props.name)) {
            return (
                <BlockEdit {...props} />
            );
        }

        const {size, className} = props.attributes;
        let classes = className;
        if (size && classes !== undefined && classes.indexOf('sizes') === false) {
            classes += ' typography--' + size;
        }
        if (className !== undefined && className.indexOf(classes) === false) {
            props.setAttributes({className: classes});
        }
        return (
            <Fragment>
                <BlockEdit {...props} />
                <InspectorControls>
                    <PanelBody
                        title={__('Typography')}
                        initialOpen={true}
                    >
                        <SelectControl
                            label={__('Select Size')}
                            value={size}
                            options={sizingControlOptions}
                            onChange={(selectedSizingOption) => {
                                props.setAttributes({
                                    size: selectedSizingOption,
                                    className: selectedSizingOption !== '' ? `typography--${selectedSizingOption}` : ''
                                });
                            }}
                        />
                    </PanelBody>
                </InspectorControls>
            </Fragment>
        );
    };
}, 'withSizingControl');
addFilter('editor.BlockEdit', 'ishtar/with-size-control', withSizingControl);

/**
 *
 * @param {object} saveElementProps Props of save element.
 * @param {Object} blockType Block type information.
 * @param {Object} attributes Attributes of block.
 *
 * @returns {object} Modified props of save element.
 */
const addSizingExtraProps = (saveElementProps, blockType, attributes) => {
    if (!enableSizingControls.includes(blockType.name)) {
        return saveElementProps;
    }

    const {className} = saveElementProps;
    const {size} = attributes;

    let classes = className;
    if (size && classes !== undefined && classes.indexOf('sizes') === false) {
        classes += ' typography--' + size;
    }
    if (className !== undefined && className.indexOf(classes) === false) {
        saveElementProps = {...saveElementProps, ...{className: classes}};
        props.setAttributes({className: classes});
    }

    return saveElementProps;
};
addFilter('blocks.getSaveContent.extraProps', 'ishtar/get-save-content/extra-props', addSizingExtraProps);
