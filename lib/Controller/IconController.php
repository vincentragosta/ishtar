<?php

namespace Ishtar\Controller;

use Ishtar\Components\Icon\IconView;

/**
 * Class IconController
 * @package Ishtar\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class IconController
{
    public function __construct()
    {
        add_shortcode('svg-icon', [$this, 'svgShortcode']);
    }

    public function svgShortcode($atts)
    {
        $atts = shortcode_atts([
            'icon' => '',
        ], $atts, 'svg-icon');

        return new IconView($atts['icon']);
    }
}