<?php

namespace Ishtar\Utility;

/**
 * Class ComponentUtility
 * @package Ishtar\Utility
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
final class ComponentUtility
{
    public static function arrayToAttributes($arr, $glue = ' ')
    {
        $ret = [];
        foreach ($arr as $name => $attr) {
            if (is_array($attr)) {
                $attr = implode($glue, $attr);
            }
            $ret[] = !is_null($attr) ? $name . '="' . esc_attr($attr) . '"' : $name;
        }

        return implode(' ', $ret);
    }

    public static function classes($base, $class_modifiers = []): string
    {
        if (empty($class_modifiers)) {
            return $base;
        }
        return sprintf(
            '%1$s %2$s',
            $base,
            implode(' ', preg_filter(
                '/^/',
                "{$base}--",
                array_filter($class_modifiers)
            ))
        );
    }

    public static function attributes($base, $class_modifiers = [], $element_attributes = []): string
    {
        return static::arrayToAttributes(
            static::attributesArray($base, $class_modifiers, $element_attributes)
        );
    }

    public static function attributesArray($base, $class_modifiers = [], $element_attributes = [])
    {
        $core = [];
        if ($classes = static::classes($base, $class_modifiers)) {
            $core['class'] = $classes;
        }
        if (isset($element_attributes['class'])) {
            $core['class'] = $core['class'] . " {$element_attributes['class']}";
            unset($element_attributes['class']);
        }
        if (!empty($element_attributes)) {
            $core = array_merge($core, $element_attributes);
        }
        return $core;
    }
}
