<?php

namespace Ishtar\Utility;

use Ishtar\View\View;

/**
 * Class ElementUtility
 * @package Ishtar\Utility
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $tag
 * @property string $content
 * @property array $attributes
 */
class ElementUtility implements View
{
    protected $tag = '';
    protected $content = '';
    protected $attributes = [];

    public function __construct(string $content = '', array $attributes = [])
    {
        $this->content = $content;
        $this->attributes = $attributes;
    }

    public static function create(string $tag, $content = '', array $attributes = [])
    {
        $element = new self($content, $attributes);
        if ($tag && !$element->tag) {
            $element->tag = $tag;
        }
        return $element;
    }

    public function getName(): string
    {
        return $this->tag;
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getScope(): array
    {
        return get_object_vars($this);
    }

    public function __toString()
    {
        return $this->tag ? static::wrapElement($this->content, $this->tag, $this->attributes) : $this->content;
    }

    public static function wrapElement($content, $tag, $attributes = [])
    {
        $attributes = ComponentUtility::arrayToAttributes($attributes);
        return $content = sprintf(
            '<%1$s%2$s>%3$s</%1$s>',
            $tag,
            $attributes ? ' ' . $attributes : '',
            $content
        );
    }

    public function hasTag()
    {
        return !empty($this->getName());
    }

    public function isTag($tag)
    {
        return $tag == $this->getName();
    }

    public function transform(string $tag)
    {
        return static::create($tag, $this->content, $this->attributes);
    }

    public function content(string $content = ''): self
    {
        $element = clone $this;
        $element->content = $content;
        return $element;
    }

    public function attribute(string $key, $value = ''): self
    {
        return $this->attributes([$key => $value]);
    }

    public function attributes(array $attributes = []): self
    {
        $element = clone $this;
        $element->attributes = array_merge($element->attributes, $attributes);
        return $element;
    }

    public function addClass($class_to_add)
    {
        $classes = $this->getNormalizedClass();
        $classes[] = $class_to_add;
        return $this->attribute('class', array_unique($classes));
    }

    public function removeClass($class_to_remove)
    {
        $classes = $this->getNormalizedClass();
        $classes_map = array_combine($classes, $classes);
        if (isset($classes_map[$class_to_remove])) {
            unset($classes_map[$class_to_remove]);
        }
        return $this->attribute('class', array_unique(array_values($classes_map)));
    }

    public function __call($name, $arguments): self
    {
        return $this->attribute($name, $arguments[0]);
    }

    public function __invoke(string $content = '', $attributes = [])
    {
        $element = $this->content($content);
        if (!empty($attributes)) {
            $element = $element->attributes($attributes);
        }
        return $element;
    }

    protected function getNormalizedClass()
    {
        $classes = $this->attributes['class'] ?? [];
        if (!is_array($classes)) {
            $classes = explode(' ', $classes);
        }
        return $classes;
    }

}