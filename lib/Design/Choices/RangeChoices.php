<?php

namespace Ishtar\Design\Choices;

use Ishtar\Design\Customize\CustomizeChoices;

/**
 * Class RangeChoices
 * @package Ishtar\Design\Choices
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class RangeChoices extends CustomizeChoices
{
    protected $format = '%s';

    public function __construct($start, $end, $step = 1, $format = '%s', $format_labels_only = false)
    {
        $values = range($start, $end, $step);
        $this->format = $format;
        $labels = $this->format($values);
        if (!$format_labels_only) {
            $values = $this->format($values);
        }
        parent::__construct(array_combine($values, $labels));
    }

    public static function pixels($start, $end, $step = 1)
    {
        return new static($start, $end, $step, '%spx');
    }

    public static function ems($start, $end, $step = 1)
    {
        return new static($start, $end, $step, '%sem');
    }

    public static function rems($start, $end, $step = 1)
    {
        return new static($start, $end, $step, '%srem');
    }

    protected function format($array)
    {
        return array_map(function($value) {
            return sprintf($this->format, $value);
        }, $array);
    }

    public function getFormat(): string
    {
        return $this->format;
    }
}
