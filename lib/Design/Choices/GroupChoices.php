<?php


namespace Ishtar\Design\Choices;

use Ishtar\Design\Customize\CustomizeChoices;
use Ishtar\Design\Customize\CustomizeGroup;

/**
 * Class GroupChoices
 * @package Ishtar\Design\Choices
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class GroupChoices extends CustomizeChoices
{
    protected $group;

    public function __construct(CustomizeGroup $group, array $static_items = [])
    {
        $this->group = $group;
        parent::__construct($static_items);
    }

    public function getChoices(): array
    {
        $group_choices = array_combine(
            $this->group->mapMethod('getId'),
            $this->group->mapMethod('getLabel')
        );
        return array_merge($this->choices, $group_choices);
    }


}
