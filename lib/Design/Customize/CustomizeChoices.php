<?php

namespace Ishtar\Design\Customize;

/**
 * Class CustomizeChoices
 * @package Ishtar\Design\Customize
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class CustomizeChoices
{
    protected $choices = [];

    public function __construct(array $choices = [])
    {
        $this->choices = $choices;
    }

    public function getChoices(): array
    {
        return $this->choices;
    }

    public function prepend($choices)
    {
        $this->choices = $choices + $this->choices;
        return $this;
    }

    public function append($choices)
    {
        $this->choices = $this->choices + $choices;
        return $this;
    }

}
