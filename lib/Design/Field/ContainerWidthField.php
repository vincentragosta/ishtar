<?php

namespace Ishtar\Design\Field;

use Ishtar\Design\Customize\CustomizeField;

/**
 * Class ContainerWidthField
 * @package Ishtar\Design\Field
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class ContainerWidthField extends CustomizeField
{
    public function getProperties()
    {
        $properties = parent::getProperties();
//        $multiplier = $this->Parent->getValueFromRoot('global__sizing__spacer-root');
        $layout_spacing = $this->Parent->getValueFromRoot('global__sizing__layout-spacing', true);
//        $padding = $multiplier * $layout_spacing;
        $properties['container-max-width'] = $this->getValue(true) + $layout_spacing . 'px';
        return $properties;
    }
}
