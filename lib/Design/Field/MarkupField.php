<?php

namespace Ishtar\Design\Field;

use Ishtar\Design\Customize\CustomizeField;

/**
 * Class MarkupField
 * @package Ishtar\Design\Field
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class MarkupField extends CustomizeField
{
    public function __construct(string $key, array $args = [])
    {
        $args['transport'] = 'refresh';
        parent::__construct($key, $args);
    }

    public function getProperties()
    {
        return [];
    }
}

