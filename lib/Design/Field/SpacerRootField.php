<?php

namespace Ishtar\Design\Field;

use Ishtar\Design\Customize\CustomizeField;

/**
 * Class SpacerRootField
 * @package Ishtar\Design\Field
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class SpacerRootField extends CustomizeField
{
    const BASE = 10;

    public function getProperties()
    {
        $key = $this->getKey();
        $max = self::BASE * floatval($this->getValue());
        $properties = [
            "$key--default" => static::BASE . 'px',
            "$key--max" => $max . 'px',
        ];

        $properties["$key--diff"] = $max - static::BASE;
        return $properties;
    }
}
