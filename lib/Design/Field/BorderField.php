<?php

namespace Ishtar\Design\Field;

use Ishtar\Design\Choices\RangeChoices;
use Ishtar\Design\Customize\CustomizeField;

/**
 * Class BorderField
 * @package Ishtar\Design\Field
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class BorderField extends ParentField
{
    protected function getChildFields(array $args = []): array
    {

        return [
            new CustomizeField('width', $this->appendLabel(array_merge([
                'description' => 'Set the border width',
                'default' => $args['default']['width'] ?? '0px',
                'choices' => RangeChoices::pixels(0, 5, 1)
            ], $args), 'Width')),
            new ColorSelectionField('color', $this->appendLabel(array_merge([
                'description' => 'Set the border color',
                'default' => $args['default']['color'] ?? 'inherit',
            ], $args), 'Color')),
            new CustomizeField('radius', $this->appendLabel(array_merge([
                'description' => 'Corner curvature (1em = 1x font size)',
                'default' => $args['default']['radius'] ?? '0.25em',
                'choices' => RangeChoices::ems(0, 2, 0.25)
            ], $args), 'Radius'))

        ];
    }
}
