<?php

namespace Ishtar\Design\Field;

use Ishtar\Design\Customize\CustomizeField;

/**
 * Class SpacingField
 * @package Ishtar\Design\Field
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class SpacingField extends CustomizeField
{
    public function getProperties()
    {
        $key = $this->getKey();
        $default = $this->getValue(true);
//        $default = $this->getValue(true) / SpacerRootField::BASE;
//        var_dump(SpacerRootField::);
        return [
            "$key--default" => $default . 'rem',
            "$key--quarter" => $default / 4 . 'rem',
            "$key--half" => $default / 2 . 'rem',
            "$key--double" => $default * 2 . 'rem',
        ];
    }
}
