<?php

namespace Ishtar\View;

/**
 * Interface View
 * @package Ishtar\View
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
interface View
{
    public function __toString();

    public function getName(): string;

    public function getScope(): array;
}
