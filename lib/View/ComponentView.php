<?php

namespace Ishtar\View;

use ReflectionClass, Throwable;

/**
 * Class ComponentView
 * @package Ishtar\View
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property array $class_modifiers
 * @property array $element_attributes
 */
abstract class ComponentView extends TemplateView
{
    static private $component_dirs;
    static private $component_default_properties = [
        'class_modifiers' => [],
        'element_attributes' => []
    ];
    static protected $default_properties = [];

    public function __construct(array $properties = [])
    {
        parent::__construct($this->mergeProperties($properties));
    }

    protected function setDefaultTemplate()
    {
        $class_name = get_class($this);
        if (empty(self::$component_dirs[$class_name])) {
            try {
                $reflector = new ReflectionClass($class_name);
                self::$component_dirs[$class_name] = dirname($reflector->getFileName());
            } catch (Throwable $e) {
                error_log($e->getMessage());
                return;
            }
        }
        $this->default_template = sprintf('%s/%s', self::$component_dirs[$class_name], $this->getName());
    }

    /**
     * @param array|string $class_modifiers
     * @return $this
     */
    public function classModifiers($class_modifiers)
    {
        $this->mergeArrayProperty('class_modifiers', $class_modifiers);
        return $this;
    }

    /**
     * @param array|string $element_attributes
     * @return $this
     */
    public function elementAttributes($element_attributes)
    {
        $this->mergeArrayProperty('element_attributes', $element_attributes);
        return $this;
    }

//    protected function mergeArrayProperty($property_name, $values)
//    {
//        if (!is_array($values)) {
//            $values = [$values];
//        }
//        $this->{$property_name} = array_merge($this->{$property_name}, $values);
//    }

    protected function mergeProperties($properties)
    {
        return array_merge(
            self::$component_default_properties,
            static::$default_properties,
            $properties
        );
    }

//    protected static function isBlockDefaultProperty($key)
//    {
//        return isset(self::$component_default_properties[$key]);
//    }
}
