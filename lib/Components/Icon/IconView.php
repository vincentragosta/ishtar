<?php

namespace Ishtar\Components\Icon;

use Ishtar\View\ComponentView;

/**
 * Class IconView
 * @package Ishtar\Components\Icon
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class IconView extends ComponentView
{
    protected $name = 'icon';
    protected static $default_properties = [
        'icon_name' => ''
    ];

    public function __construct(string $icon_name)
    {
        if (strpos($icon_name, 'icon-') === false) {
            $icon_name = sprintf('icon-%s', $icon_name);
        }
        parent::__construct(compact('icon_name'));
    }
}