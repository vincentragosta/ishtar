<?php
/**
 * Expected:
 * @var string $icon_name
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Ishtar\Utility\ComponentUtility;

?>

<svg <?= ComponentUtility::attributes('icon', $class_modifiers, $element_attributes); ?>>
    <use xlink:href="#<?= $icon_name; ?>"></use>
</svg>
